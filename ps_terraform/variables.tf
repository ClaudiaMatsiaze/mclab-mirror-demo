variable "region" {
    type = string
}

variable "availability_domain" {
    type = string
    description = "Availability Domain for Instances"
}

variable "compartment_id" {
    type = string
    description = "Compartment ID"
}

variable "boot_volume_backup_source_id"{
    type = string
    description = "ID Boot volume backup source "
}

variable "boot_volume_backup_source_type" {
    type = string
    description = "Type Boot volume backup"
}

variable "boot_volume_display_name"{
    type = string
    description = "Boot volume names"
}

variable "subnet_id" {
    type = string
    description = "Public Subnet ID"
}

variable "instance_shape" {
    type = string
    description = "Instance Shape"
}

variable "instance_shape_config_ocpus" {
    type = string
    description = "Instance Shape Config OCPUs"
}

variable "instance_shape_config_memory_in_gbs" {
    type = string
    description = "Instance Shape Config Memory in GBs"
}

variable "instance_count" {
  type = number
  description = "Number of instances to provision"
}

variable "instance_display_name"{
    type = string
    description = "Instance names"
}

variable "vnic_display_name"{
    type = string
    description = "Vnic hostname label"
}
