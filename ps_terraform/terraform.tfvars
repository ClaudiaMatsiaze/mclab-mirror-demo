# Variables Globales
region = "eu-paris-1"
availability_domain = "TNVw:EU-PARIS-1-AD-1"
compartment_id = "ocid1.compartment.oc1..aaaaaaaamtkb5nh5nj53pj677tvyqx3hbhk7keramsitiffj57iccpxwumoa"

# Boot Volume Backup Source Details
boot_volume_backup_source_id = "ocid1.bootvolumebackup.oc1.eu-paris-1.abrwiljrwsv3xwo2t42axykrenccii2okyhorpxdpvmgja7sltr7hojaen4a"
boot_volume_backup_source_type = "bootVolumeBackup"

## (Note: Change the 4 last digits in all display names with the current MMYY: 0223 = Fevrier 2023)

# Boot Volume 
boot_volume_display_name = "BOOT_VOLUME_0223"

# Instances
instance_count = 3
instance_shape = "VM.Standard.E4.Flex"
instance_display_name ="PSFT_FSCM_0223"
instance_shape_config_ocpus = "1"
instance_shape_config_memory_in_gbs = "16"

vnic_display_name = "psft-fscm-0223"
subnet_id = "ocid1.subnet.oc1.eu-paris-1.aaaaaaaaxpqqolimncnsph7q64uukh3m77id6j6vb66zsb4sbl6u776hjwfa"
